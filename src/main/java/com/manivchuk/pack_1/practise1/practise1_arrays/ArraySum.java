package com.manivchuk.pack_1.practise1.practise1_arrays;

public class ArraySum {
    public static int sum(int[] array){
        int sum = 0;
        for(int i = 0; i < array.length;i++){
            sum += array[i];
        }
        return sum;
    }
}
