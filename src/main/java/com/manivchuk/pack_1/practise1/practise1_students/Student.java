package com.manivchuk.pack_1.practise1.practise1_students;

import java.util.ArrayList;

public class Student {
    private String name;
    private String surname;

    private ArrayList<Exam> allExam = new ArrayList<>();

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public ArrayList<Exam> getAllExam() {
        return allExam;
    }

    public void setAllExam(ArrayList<Exam> allExam) {
        this.allExam = allExam;
    }

    @Override
    public String toString() {
        return name +" "+ surname;
    }

    public void addRatingForExam(Exam subject) {
        allExam.add(subject);
    }

}
