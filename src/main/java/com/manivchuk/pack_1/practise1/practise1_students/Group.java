package com.manivchuk.pack_1.practise1.practise1_students;

public class Group {
    private int course;
    private String faculty;

    public Group(int course, String faculty) {
        this.course = course;
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "course=" + course +
                ", faculty='" + faculty + '\'';
    }
}
