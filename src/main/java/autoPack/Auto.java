package autoPack;

public class Auto {
    String color;

    public Auto(String color) {
        this.color = color;
    }

    public void color(){
        color = "red";
    }

    @Override
    public String toString() {
        return "Auto{" +
                "color='" + color + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Auto)) return false;

        Auto auto = (Auto) o;

        return !(color != null ? !color.equals(auto.color) : auto.color != null);

    }

}

class Test{
    public static void main(String[] args) {
        Auto a1 = new Auto("Green");
        Auto a2 = new Auto("Green");
        System.out.println(a1.equals(a2));


        Auto a3 = new Auto("Red"){
            @Override
            public String toString() {
                return "AnonimAuto{" +
                        "color='" + color + '\'' +
                        '}';
            }
            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Auto)) return false;

                Auto auto = (Auto) o;

                return !(color != null ? !color.equals(auto.color) : auto.color != null);

            }

        };
        System.out.println(a3);

        Auto a4 = new Auto("Red"){
            @Override
            public String toString() {
                return "Auto{" +
                        "color='" + color + '\'' +
                        '}';
            }
        };

        System.out.println(a3.equals(a4));
    }
}
